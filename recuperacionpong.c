/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by Sara Zafra
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................
    // NOTE: Here there are some useful variables (should be initialized)
    bool pause = false;
    
    bool fadeIn = true;
    float alpha = 0;
    float fadeSpeed = 0.01f;
    const char start [15] = "PRESS ENTER";
    bool drawEnter = false;
    bool incrementa = true;
    
    
    int width = 20;
    int height = 80;
    
    int posx = screenWidth/2;
    int posy = screenHeight/2;
    
    int playerLife = 200;
    int enemyLife = 200;
    
    Rectangle player;
    player.x = 0;
    player.y = posy;
    player.width = width;
    player.height = height;
    int playerSpeedY = 4;
    
    Rectangle enemy;
    enemy.x = 780;
    enemy.y = posy;
    enemy.width = width;
    enemy.height = height;
    
    int visionEnemy = screenWidth - 500;
    int enemySpeedY = 4;
    
    Vector2 Ball;    
    Ball.x = screenWidth/2;
    Ball.y = screenHeight/2;
    Vector2 BallSpeed = {4,5};
   
    float BallRadius = 20;
    
        
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    const char msg2[50] = "FINAL PONG ";
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    
    Image image = LoadImage("pika.png");     // Loaded in CPU memory (RAM)
    Texture2D texture = LoadTextureFromImage(image);    // Image converted to texture, GPU memory (VRAM)
    UnloadImage(image);
   
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                // TODO: Logo fadeIn and fadeOut logic...............
                
            if(fadeIn)
            {
                alpha += fadeSpeed;
                           
                if(alpha >= 1.0f)
                {
                    alpha = 1.0f;
                }
            }
            else
            {
                alpha -= fadeSpeed;
                if(alpha <= 0.0f)
                {
                    alpha = 0.0f;
                }
            }
       
            
            if(framesCounter > 120){
                fadeIn = false;
            }
            
            
             if(framesCounter > 240) 
             {
                screen = TITLE;
             }
             framesCounter++;
                                             
                
            }break;
            
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................
            if(framesCounter > 140)      
            {
                fadeIn = true;
            }                
             
            if(fadeIn)
            {
                alpha += fadeSpeed;
                           
                if(alpha >= 1.0f)
                {
                    alpha = 1.0f;
                }
            }
            
                // TODO: "PRESS ENTER" logic.........................
                framesCounter ++;
            if(framesCounter >= 30)
            {
            
             framesCounter = 0;
             drawEnter = !drawEnter;
             }
             if (IsKeyPressed(KEY_ENTER)) screen = GAMEPLAY;
                
            } break;
            
            
            case GAMEPLAY:
            { 
            
                // Update GAMEPLAY screen data here!
                if (IsKeyPressed(KEY_ENTER)) screen = ENDING;
                

                // TODO: Ball movement logic.........................
                 Ball.x += BallSpeed.x;
                 Ball.y += BallSpeed.y;
        
                if (((Ball.x + BallRadius) >= screenWidth) || ((Ball.x - BallRadius) <= 0)) BallSpeed.x *= -1;
                if (((Ball.y + BallRadius) >= screenHeight) || ((Ball.y - BallRadius) <= 0)) BallSpeed.y *= -1;
                
                           
                
                // TODO: Player movement logic.......................
                if(IsKeyDown(KEY_W))  {         //subir barra player
                   incrementa =! incrementa;                            
       
       
                if(incrementa)
                    player.y = player.y - 10;
                }
        
                else{
                   player.y = player.y;
                }
                
               
                
                if(IsKeyDown(KEY_S)){          // bajar barra player
                   incrementa =! incrementa;  
          
                if(incrementa)
                   player.y = player.y + 10;
                }
                else{
                   player.y = player.y;
                }
                // TODO: Enemy movement logic (IA)...................
                if (Ball.x >= visionEnemy)
                {
                if (Ball.y > (enemy.y + enemy.height/2)) enemy.y += enemySpeedY;
                if (Ball.y < (enemy.y + enemy.height/2)) enemy.y -= enemySpeedY;
                }
        
                if (enemy.y <= 0) enemy.y = 0;
                else if ((enemy.y + enemy.height) >= screenHeight) enemy.y = screenHeight - enemy.height;
                
                // TODO: Collision detection (ball-player) logic.....
                 if (CheckCollisionCircleRec(Ball, BallRadius, player))
                 {
                 if(BallSpeed.x < 0){
                 BallSpeed.x *= -1;
                   }
                 }
                
                // TODO: Collision detection (ball-enemy) logic......
                 if (CheckCollisionCircleRec(Ball, BallRadius, enemy))
                 {
                 if(BallSpeed.x > 0){
                 BallSpeed.x *= -1;
                   }
                 }
                
                // TODO: Collision detection (ball-limits) logic.....
                 if(Ball.x <BallRadius)
                {
                  posx *= -1;
                }
       
               if(Ball.y<BallRadius)
               {
                 posy *= -1;
               }
       
               if(Ball.x>screenWidth-BallRadius)
               {
                 posx *= -1;
               }
       
               if(Ball.y>screenHeight-BallRadius)
               {
                posy *= -1;
               }
                
                // TODO: Life bars decrease logic....................
                
                if (Ball.x > (screenWidth - BallRadius))
                    {
                        enemyLife -= 10;
                        if (enemyLife <= 0) screen = ENDING;

                        Ball.x = width;
                        Ball.y = height;
                    }

                    if (Ball.x < BallRadius)
                    {
                        playerLife -= 10;
                        if (playerLife <= 0) screen = ENDING;

                        
                    } 

                // TODO: Time counter logic..........................
                framesCounter--;
       
                if ((framesCounter%90) == 0) secondsCounter--;

                // TODO: Game ending logic...........................
                if (secondsCounter == 00) screen = ENDING;
                // TODO: Pause button logic..........................
                if (IsKeyPressed (KEY_P)) pause = !pause;
                
                if (!pause){
                    Ball.x += BallSpeed.x;
                    Ball.y += BallSpeed.y;
                   } 
                
               
                
                
                     
                
            break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................
                 if (IsKeyPressed(KEY_ENTER)) screen = TITLE;
                 if (IsKeyPressed(KEY_ESCAPE)) WindowShouldClose;
                
            } break;
            default: break;
        }
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                   
                
                    // TODO: Draw Logo...............................
                   
                      ClearBackground(BLACK);

                      DrawTexture(texture, screenWidth/2 - texture.width/2, screenHeight/2 - texture.height/2, Fade(WHITE, alpha));
                      
                                     
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    ClearBackground(BLACK);
                    // TODO: Draw Title..............................
                    DrawText("FINAL PONG", 220, 100, 60, Fade(YELLOW, alpha));
                    // TODO: Draw "PRESS ENTER" message..............
                    if (drawEnter)
                    {
            
                    DrawText (start, screenWidth/2 - (MeasureText(start,20)/2), screenHeight - 200, 20, PINK);
            
                    }
                    
            
        
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    DrawRectangle(0, 0, screenWidth, screenHeight, LIGHTGRAY);                    
                    // TODO: Draw player and enemy...................
                    DrawRectangleRec(player, BLUE);
                    DrawRectangleRec(enemy, PINK); 
                    DrawCircleV(Ball, 20, YELLOW);
                    // TODO: Draw player and enemy life bars.........
                    DrawRectangle(25, 10, playerLife,30,  RED);
                    DrawRectangle(550, 10, enemyLife, 30,  RED);
                    
                                     
                    
                    // TODO: Draw time counter.......................
                      DrawText(FormatText("%02i", secondsCounter), 40, 50, 35, DARKGRAY);
                
                     
                    // TODO: Draw pause message when required........
                     if (pause)
                {
                     DrawText("GAME PAUSED", 250, 200, 40, BLACK);  
                                               
                }
            
                }break;
                case ENDING: 
                {
                    // Draw END screen here!
                    DrawRectangle(0, 0, screenWidth, screenHeight, BLUE);
                    DrawText("press ENTER for TITLE screen", 230, 200, 20, DARKBLUE);
                    // TODO: Draw ending message (win or loose)......
                    
                } break;
                default: break;
            }
        
            
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
     UnloadTexture(texture); 

    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}